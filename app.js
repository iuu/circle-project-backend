const express = require('express');
const bodyParser = require('body-parser');

const socketio = require('socket.io')()
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

const {google} = require('googleapis')
const path = require('path')
const fs = require('fs')

const CLIENT_ID = '698966640271-tiusf4g0ri9dgpg6mpt12onpkc68edu4.apps.googleusercontent.com'
const CLIENT_SECRET = '3VbJ7FYHY5uzHH69V7u1-OgJ'
const REDIRECT_URI = 'https://developers.google.com/oauthplayground'

const REFRESH_TOKEN = '1//04GrvYYpW1rGACgYIARAAGAQSNwF-L9IrYLEy2alIlwVDg-3DWtaHl7yQEySc0VMG5qmXXcz3HbS5lI1Qvnt4WJlKcXR8zydcQ2w'

const oauth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
)

oauth2Client.setCredentials({refresh_token: REFRESH_TOKEN})

const drive = google.drive({
    version: 'v3',
    auth: oauth2Client
})

const filePath = path.join(__dirname, '2021-06-14 11-48-28.mkv')

async function uploadFile(){
    try {
        const response = await drive.files.create({
            requestBody: {
                name: 'vid.mkv',
                mimeType: 'video/x-matroska'
            },
            media:{
                mimeType: 'video/x-matroska',
                body: fs.createReadStream(filePath)
            }
        })
        console.log(response.data)
    } catch (error) {
        console.log(error.message)
    }
}

async function deleteFile(){
    try {
        const response = await drive.files.delete({
            fileId: '',
        });

        console.log(response.data, response.status)
    } catch (error) {
        console.log(error.message)
    }
}

async function generatePublicUrl(){
    try {
        const fileId = '1Ubf4eIO2LMfnMZy8uXFU9IiLGIQoi0NT';
        await drive.permissions.create({
            fileId: fileId,
            requestBody:{
                role:'reader',
                type:'anyone'
            }
        })
        const result = await drive.files.get({
            fileId: fileId,
            fields: 'webViewLink, webContentLink'
        })
        console.log(result.data)
    } catch (error) {
        console.log(error.message)
    }
}
// uploadFile()
// generatePublicUrl()

var server = app.listen(3000,()=>{
    console.log('Server is running on port number 3000')
})


var io = socketio.listen(server)

io.on('connection',function(socket) {

    //The moment one of your client connected to socket.io server it will obtain socket id
    //Let's print this out.
    console.log(`Connection : SocketId = ${socket.id}`)
    //Since we are going to use userName through whole socket connection, Let's make it global.   
    var userName = '';
    
    socket.on('subscribe', function(data) {
        console.log('subscribe trigged')
        const room_data = JSON.parse(data)
        userName = room_data.userName;
        const roomName = room_data.roomName;
    
        socket.join(`${roomName}`)
        console.log(`Username : ${userName} joined Room Name : ${roomName}`)
        
       
        // Let the other user get notification that user got into the room;
        // It can be use to indicate that person has read the messages. (Like turns "unread" into "read")

        //TODO: need to chose
        //io.to : User who has joined can get a event;
        //socket.broadcast.to : all the users except the user who has joined will get the message
        // socket.broadcast.to(`${roomName}`).emit('newUserToChatRoom',userName);
        io.to(`${roomName}`).emit('newUserToChatRoom',userName);

    })

    socket.on('unsubscribe',function(data) {
        console.log('unsubscribe trigged')
        const room_data = JSON.parse(data)
        const userName = room_data.userName;
        const roomName = room_data.roomName;
    
        console.log(`Username : ${userName} leaved Room Name : ${roomName}`)
        socket.broadcast.to(`${roomName}`).emit('userLeftChatRoom',userName)
        socket.leave(`${roomName}`)
    })

    socket.on('newMessage',function(data) {
        console.log('newMessage triggered')

        const messageData = JSON.parse(data)
        const messageContent = messageData.messageContent
        const roomName = messageData.roomName

        console.log(`[Room Number ${roomName}] ${userName} : ${messageContent}`)
        
        // Just pass the data that has been passed from the writer socket
        const chatData = {
            userName : userName,
            messageContent : messageContent,
            roomName : roomName
        }
        socket.broadcast.to(`${roomName}`).emit('updateChat',JSON.stringify(chatData)) // Need to be parsed into Kotlin object in Kotlin
    })

    //If you want to add typing function you can make it like this.
    
    // socket.on('typing',function(roomNumber){ //Only roomNumber is needed here
    //     console.log('typing triggered')
    //     socket.broadcast.to(`${roomNumber}`).emit('typing')
    // })

    // socket.on('stopTyping',function(roomNumber){ //Only roomNumber is needed here
    //     console.log('stopTyping triggered')
    //     socket.broadcast.to(`${roomNumber}`).emit('stopTyping')
    // })

    socket.on('disconnect', function () {
        console.log("One of sockets disconnected from our server.")
    });
})